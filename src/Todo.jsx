import React, { useState } from 'react';
import ToDoList from './ToDoList'
import AddIcon from '@material-ui/icons/Add';
import Button from '@material-ui/core/Button';
import '../node_modules/bootstrap/dist/css/bootstrap.min.css';

const Todo = () =>{
     //use Hooks
     const [inputlist, setInputList] = useState("");
     const [Items, setItems] = useState( [] );

     const itemEvent = (event) =>{
        setInputList (event.target.value);

     };
     const listOfItems =() =>{
         setItems ((oldItems) =>{

             return [...oldItems, inputlist];
             
         });
         setInputList("");
     } 
     const deleteItems = (id) =>{
       // console.log('Deleted');
        setItems ((oldItems) => {
            return oldItems.filter((arrElem, index) => {
                return index !==id;
            })
        })
    };
    
    return (
        <>
        <div className = 'main_div'> 
            <div className = 'center_div'>
            <br/>
            <h1> ToDo List</h1>
            <br/>
             <input type = "text" name = "item" placeholder = "Add Items" value = {inputlist}  onChange = {itemEvent} />
        <button  onClick = {listOfItems}> <AddIcon/> </button>
        

        <ol>
           {/* <li onClick = {listOfItems}> {inputlist} </li> */}
           {Items.map( (itemVal, index) => {
               return <ToDoList key ={index} id ={index} text ={itemVal} onSelect = {deleteItems} /> 
           })}
        </ol>
        </div>
        </div>

        </>
    );

};
export default Todo;
