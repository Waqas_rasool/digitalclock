import React, { useState } from 'react';

const Form = () =>{

   

    const [fullName, setFullName] = useState({
        fname: "",
        lname: "",
        email: "",
        phone: "",
    });

    //const [lastName, setLastNmae] = useState("");
    //const [fullName, setFullName] = useState();
    //const [lastNameNew, setLastNameNew] = useState();
    const onSubmit = (event) =>{
    event.preventDefault();
    alert("Form Submited");
    }


    const InputEvent = (event) =>{
       console.log(event.target.value);
       console.log(event.target.name);

       //const value = event.target.value;
       //const name = event.target.name;

       // Object Distructing
       const {value, name} = event.target;

       setFullName((preValue) =>{
           console.log(preValue);
           
           //Use spread operator
          return{
              ...preValue,
              [name]: value,
             
          };
          
          
          
          /*if(name === 'fName'){
            return{
                fname: value,
                lname: preValue.lname,
                email: preValue.email,
                phone: preValue.phone,
                }
           }else if(name === 'lName'){
            return{
                fname: preValue.fname,
                lname: value,
                email: preValue.email,
                phone: preValue.phone,
                }
        } else if(name === 'email'){
            return{
                fname: preValue.fname,
                lname: preValue.lname,
                email: value,
                phone: preValue.phone,
                }
        } else if(name === 'phone'){
            return{
                fname: preValue.fname,
                lname: preValue.lname,
                email: preValue.email,
                phone: value,
                }
        }*/
       });  
       
        
    };
    
    return(
    <>
    <form onSubmit={onSubmit}>
    <div>
        <h1>Hello {fullName.fname} {fullName.lname}</h1>
        <p>{fullName.email}</p>
        <p>{fullName.phone}</p>
        <input type = "text" placeholder = "Enter Your First Name" name = "fname" onChange={InputEvent} value ={fullName.fname} autoComplete= "off" />
        
       <input type = "text" placeholder = "Enter Your Last Name" name = "lname" onChange={InputEvent} value ={fullName.lname} autoComplete= "off"/>

       <input type = "text" placeholder = "Enter Your Email Address" name = "email" onChange={InputEvent} value ={fullName.email} autoComplete= "off"/>

       <input type = "number" placeholder = "Enter Your Phone Number" name = "phone" onChange={InputEvent} value ={fullName.phone} autoComplete= "off"/>
      

      
       
        <button type= "submit"> Submit</button>
    </div>    
    </form>
    </>
    );
};
export default Form;